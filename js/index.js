"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

//Додавання унікального номеру кожному обєкту-----------------------------------------------------------------------------

let cardNumber = 0;
DATA.forEach((el) => {
	el.cardNumber = cardNumber;
	cardNumber++;
})



const cardsContainer = document.querySelector(".trainers-cards__container");
const trainerCard = document.querySelector("#trainer-card").content;
const sectionSorting = document.querySelector(".sorting");
const asideSidebar = document.querySelector(".sidebar");
const windowModalTemplate = document.querySelector("#modal-template").content;
const submitButton = document.querySelector(".filters__submit");


//ввиведення карток-----------------------------------------------------------


const showingTrainer = (data)=> {
	cardsContainer.innerHTML = "";
	let cartId = 0;


	data.forEach(el => {
		el.id = cartId;
		cartId++;

		const card = trainerCard.cloneNode(true);
		card.querySelector(".trainer__show-more").setAttribute("data-id", el.id);
		card.querySelector(".trainer__name").innerText = `${el["last name"]} ${el["first name"]}`;
		card.querySelector("img").src = el.photo;
		cardsContainer.append(card);
	});
};

showingTrainer(DATA);

sectionSorting.removeAttribute("hidden");
asideSidebar.removeAttribute("hidden");



//сортування-----------------------------------------------


const sortFunction = (value, data) => {

	switch (value) {
		case "ЗА ПРІЗВИЩЕМ":{
			showingTrainer(data.sort((a,b) => a["last name"].localeCompare(b["last name"])));
			break;
		}
		case "ЗА ДОСВІДОМ":{
			showingTrainer(data.sort((a,b) => b.experience.localeCompare(a.experience)));
			break;
		}
		case "ЗА ЗАМОВЧУВАННЯМ": {
			showingTrainer(data.sort((a, b) => a.cardNumber - b.cardNumber));
			break;
		}
	}
};


//фільтрація------------------------------------------------------------


const filterFunctionForDirection = (value) => {

	let filteredData

	switch (value) {
		case "all-direction": {
			filteredData = DATA.filter((elem) => true);
			break;
		}
		case "gym": {
			filteredData = DATA.filter((elem) => elem.specialization === "Тренажерний зал");
			break;
		}
		case "fight-club": {
			filteredData = DATA.filter((elem) => elem.specialization === "Бійцівський клуб");
			break;
		}
		case "kids-club": {
			filteredData = DATA.filter((elem) => elem.specialization === "Дитячий клуб");
			break;
		}
		case "swimming-pool": {
			filteredData = DATA.filter((elem) => elem.specialization === "Басейн");
			break;
		}
	}
	return filteredData;
}



let filterArrayCategories;



submitButton.addEventListener("click",(event) =>{
	event.preventDefault();


	const allInput = document.querySelector(".filters");
	let checked = allInput.querySelectorAll("input:checked");
	let checkedId = Array.from(checked);



	let filterArrayDirection = filterFunctionForDirection(checkedId[0].id);


	const filterFunctionForCategories = (secondValue)=>{
		let filteredData;
		switch (secondValue) {
			case "all-category":{
				filteredData = filterArrayDirection.filter((elem) => true);
				break;
			}
			case "master":{
				filteredData = filterArrayDirection.filter((elem) => elem.category === "майстер");
				break;
			}
			case "specialist": {
				filteredData = filterArrayDirection.filter((elem) => elem.category === "спеціаліст");
				break;
			}
			case "instructor": {
				filteredData = filterArrayDirection.filter((elem) => elem.category === "інструктор");
				break;
			}
		}
		return filteredData;
	}

	filterArrayCategories = filterFunctionForCategories(checkedId[1].id);

	showingTrainer(filterArrayCategories);
})


//прослуховувач на кнопки сортування-------------------------------------------------------------------


sectionSorting.addEventListener("click", (event) => {
	if (event.target === event.currentTarget){
		return;
	}

	event.target.closest(".sorting__btn");
	document.querySelector(".sorting__btn--active").classList.remove("sorting__btn--active");
	event.target.classList.add("sorting__btn--active");

	if(filterArrayCategories){
		sortFunction(event.target.innerText, filterArrayCategories);
	}else {
		sortFunction(event.target.innerText, DATA);
	}
});


//модальне вікно----------------------------------------------------------------


let buildWindowCard = (value, target)=>{
	const windowCard = windowModalTemplate.cloneNode(true);

	windowCard.querySelector("img").src = value[target].photo;
	windowCard.querySelector(".modal__name").innerText = `${value[target]["last name"]} ${value[target]["first name"]}`;
	windowCard.querySelector(".modal__point--category").innerText = `Категорія: ${value[target].category}`;
	windowCard.querySelector(".modal__point--experience").innerText = `Досвід: ${value[target].experience}`;
	windowCard.querySelector(".modal__point--specialization").innerText = `Напрям тренера: ${value[target].specialization}`;
	windowCard.querySelector(".modal__text").innerText = value[target].description;

	return windowCard;
};


cardsContainer.addEventListener("click", (event) => {
	if (event.target === event.currentTarget){
		return;
	}


	event.target.closest(".trainer__show-more");
	let eventTarget = event.target.dataset.id;

	if(filterArrayCategories){
		document.body.prepend(buildWindowCard(filterArrayCategories, eventTarget));
	}else {
		document.body.prepend(buildWindowCard(DATA, eventTarget));
	}


	//scroll fix--------------------

	let scrollPosition = null;

	const disableScroll = function() {
		const body = document.body;
		scrollPosition = window.scrollY;

		body.style.overflow = 'hidden';
		body.style.position = 'fixed';
		body.style.width = '100%';
		body.style.top = `-${scrollPosition}px`;
	};

	disableScroll();


	//кнопка закриття модального вікна


	const modalTemplateClose = document.querySelector(".modal__close");
	modalTemplateClose.addEventListener("click", () => {
		document.querySelector(".modal").remove();


		//return scroll-------


		const enableScroll = function () {
			const body = document.body;

			body.style.overflow = '';
			body.style.position = '';
			body.style.width = 'auto';
			body.style.top = '';

			window.scrollTo(0, scrollPosition);
		};

		enableScroll();

	});
});

